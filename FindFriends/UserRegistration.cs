﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace FindFriends
{
    enum StagesUserRegistration
    {
        SetName = 1,
        SetAge,
        SetCountry,
        SetCity,
        SetGender,
        SetPhoto,
        SetDescription
    }

    enum UserParameters
    {
        name = 1,
        age,
        country,
        city,
        gender,
        photo,
        description
    }

    class UserRegistration
    {
        private User newUser;

        private readonly Dictionary<int, string> registrationStages = new Dictionary<int, string>
        {
            { 1, "Введите имя" },
            { 2, "Введите возраст" },
            { 3, "Введите страну" },
            { 4, "Введите город" },
            { 5, "Введите пол" },
            { 6, "Отправьте фото для анкеты" },
            { 7, "Опишите себя (не более 1000 символов)" }
        };

        private readonly Dictionary<int, string> messageForNotValidParameters = new Dictionary<int, string>
        {
            { 1, "Вы ввели неверное имя. Имя не может быть слишком длинным и не может иметь пробелы." },
            { 2, "Вы ввели неверный возраст. Возраст не может быть меньше 0 и быть слишком большим. Так же возраст не может содержать символы." },
            { 3, "Вы ввели страну, которой не существует. Проверьте, правильны ли вы всё написали." },
            { 4, "К сожалению, база данных городов у нас ещё не заполнена. Напишите другой город для продолжения." },
            { 5, "Вы не верно ввели пол. Пожалуйста, нажмите на кнопку с нужным полом." },
            { 6, "Вы должны отправить фото." },
            { 7, "Вы превысили лимит символов (1000)." }
        };

        private ReplyKeyboardMarkup keyboard = new ReplyKeyboardMarkup
        {
            Keyboard = new KeyboardButton[][]
                    {
                        new KeyboardButton[]
                        {
                            new KeyboardButton("Парень"),
                            new KeyboardButton("Девушка")
                        },
                    },
            ResizeKeyboard = true
        };

        public UserRegistration(string telegramId, string telegramUsername, string telegramChatId)
        {
            newUser = new User(telegramId, telegramUsername, telegramChatId);
        }

        public async Task StartRegistration(TelegramBotClient bot)
        {
            Console.WriteLine("Insert user {0} in the database. ChatId: {1}", newUser.TelegramUsername, newUser.TelegramChatId);

            MyDataBase.ExecuteQueryWithoutAnswer($"INSERT INTO User (telegram_id, telegram_username, telegram_chat_id, registration_stage) VALUES ('{newUser.TelegramId}', '{newUser.TelegramUsername}', '{newUser.TelegramChatId}', 1);");

            await bot.SendTextMessageAsync(newUser.TelegramChatId, registrationStages.Single(registrationStage => registrationStage.Key == (int)StagesUserRegistration.SetName).Value);
        }

        public async Task SetParametersForUser(string message, PhotoSize[] photos, int stage, TelegramBotClient bot)
        {
            if (stage == (int)StagesUserRegistration.SetName)
            {
                if (IsNameValid(message)) SetParameter(message, UserParameters.name);
                else { SendMessageForNotValidParameters(bot, UserParameters.name); return; }
            }
            else if (stage == (int)StagesUserRegistration.SetAge)
            {
                if (IsAgeValid(message)) SetParameter(message, UserParameters.age);
                else { SendMessageForNotValidParameters(bot, UserParameters.age); return; }
            }
            else if (stage == (int)StagesUserRegistration.SetCountry)
            {
                if (IsCountyValid(message)) SetParameter(message, UserParameters.country);
                else { SendMessageForNotValidParameters(bot, UserParameters.country); return; }
            }
            else if (stage == (int)StagesUserRegistration.SetCity)
            {
                if (IsCityValid(message)) SetParameter(message, UserParameters.city);
                else
                {
                    if(message.Length < 100) MyDataBase.ExecuteQueryWithoutAnswer($"INSERT INTO possible_city VALUES ('{message}');");
                    SendMessageForNotValidParameters(bot, UserParameters.city);
                    return;
                }
            }
            else if (stage == (int)StagesUserRegistration.SetGender)
            {
                if (IsGenderValid(message)) SetParameter(message, UserParameters.gender);
                else { SendMessageForNotValidParameters(bot, UserParameters.gender); return; }
            }
            else if (stage == (int)StagesUserRegistration.SetPhoto)
            {
                if (photos != null) SetParameter(photos[0].FileId, UserParameters.photo);
                else { SendMessageForNotValidParameters(bot, UserParameters.photo); return; }
            }
            else if (stage == (int)StagesUserRegistration.SetDescription)
            {
                if (IsDescriptionValid(message)) SetParameter(message, UserParameters.description);
                else { SendMessageForNotValidParameters(bot, UserParameters.description); return; }
                await bot.SendTextMessageAsync(newUser.TelegramChatId, "Регистрация успешно закончена.");
            }

            MyDataBase.ExecuteQueryWithoutAnswer($"UPDATE User SET registration_stage = {stage + 1} WHERE telegram_id = '{newUser.TelegramId}';");

            if (stage < 7)
            {
                // Показываем клавиатуру, когда нужно выбрать пол
                if (stage == 4) await bot.SendTextMessageAsync(newUser.TelegramChatId, registrationStages.Single(registrationStage => registrationStage.Key == stage + 1).Value, replyMarkup: keyboard);
                else await bot.SendTextMessageAsync(newUser.TelegramChatId, registrationStages.Single(registrationStage => registrationStage.Key == stage + 1).Value, replyMarkup: new ReplyKeyboardRemove());
            }
        }

        #region Validation of parameters
        private bool IsNameValid(string name)
        {
            if (name.Length > 50 || name.Contains(' ')) return false;
            return true;
        }

        private bool IsAgeValid(string age)
        {
            int result;
            if (int.TryParse(age, out result) && result > 0 && result < 150) return true;
            return false;
        }

        private bool IsCountyValid(string country)
        {
            if (MyDataBase.ExecuteQueryWithAnswer($"SELECT LOWER(name) FROM country WHERE name = '{country}';") == country.ToLower()) return true;
            return false;
        }

        private bool IsCityValid(string city)
        {
            if (MyDataBase.ExecuteQueryWithAnswer($"SELECT LOWER(name) FROM city WHERE name = '{city}';") == city.ToLower()) return true;
            return false;
        }

        private bool IsGenderValid(string gender)
        {
            if (gender == "Парень" || gender == "Девушка") return true;
            return false;
        }

        private bool IsDescriptionValid(string description)
        {
            if (description.Length < 1000) return true;
            return false;
        }
        #endregion

        private void SetParameter(string parameter, UserParameters parameterDB)
        {
            Console.WriteLine("Set {0} for the user {1}. ChatId: {2}", parameterDB, newUser.TelegramUsername, newUser.TelegramChatId);
            MyDataBase.ExecuteQueryWithoutAnswer($"UPDATE User SET {parameterDB} = '{parameter}' WHERE telegram_id = '{newUser.TelegramId}';");
        }

        private async void SendMessageForNotValidParameters(TelegramBotClient bot, UserParameters parameterDB)
        {
            Console.WriteLine("Don't Set {0} for the user {1}. ChatId: {2}. Because {0} is not valid.", parameterDB, newUser.TelegramUsername, newUser.TelegramChatId);
            await bot.SendTextMessageAsync(newUser.TelegramChatId, messageForNotValidParameters.Single(messageForNotValidParameters => messageForNotValidParameters.Key == (int)parameterDB).Value, replyMarkup: new ReplyKeyboardRemove());
        }
    }

}

