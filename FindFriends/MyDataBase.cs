﻿using System;
using System.Data;
using MySql.Data.MySqlClient;

namespace FindFriends
{
    static class MyDataBase
    {

        private static MySqlConnection connection;
        private static MySqlCommand command;

        private static void OpenConnection()
        {
            connection = new MySqlConnection(@"");
            command = new MySqlCommand("", connection);
            connection.Open();
        }
        public static void CloseConnection()
        {
            connection.Close();
            command.Dispose();
        }

        public static void ExecuteQueryWithoutAnswer(string query)
        {
            Console.WriteLine("Query: {0}", query);

                OpenConnection();

                command.CommandText = query;
                command.ExecuteNonQuery();

                CloseConnection();
        }
        // Возращает значение первой записи первого столбца вызванной таблицы
        public static string ExecuteQueryWithAnswer(string query)
        {
            OpenConnection();

            command.CommandText = query;
            var answer = command.ExecuteScalar();

            CloseConnection();

            if (answer != null) return answer.ToString();
            else return null;
        }

        public static DataTable GetTable(string query)
        {
            OpenConnection();

            MySqlDataAdapter adapter = new MySqlDataAdapter(query, connection);

            DataSet DS = new DataSet();
            adapter.Fill(DS);
            adapter.Dispose();

            CloseConnection();

            return DS.Tables[0];
        }

        public static bool IsUserRegistered(string TelegramId)
        {
            if (ExecuteQueryWithAnswer($"SELECT * FROM User WHERE telegram_id = '{TelegramId}';") != null)
            {
                if(int.Parse(ExecuteQueryWithAnswer($"SELECT registration_stage FROM User WHERE telegram_id = '{TelegramId}';")) == 8) return true;
            }
            return false;
        }

        public static User GetRandomUser()
        {
            // Получаем всех зарегистрированных пользователей
            DataTable allTelegramId = GetTable("SELECT telegram_id FROM User WHERE registration_stage = 8;");
            Random rnd = new Random();
            string randomTelegramId = allTelegramId.Rows[rnd.Next(0, allTelegramId.Rows.Count)][0].ToString();
            DataRow userInformationRow = GetTable($"SELECT * FROM User WHERE telegram_id = '{randomTelegramId}';").Rows[0];
            return new User(userInformationRow[0].ToString(), userInformationRow[1].ToString(), userInformationRow[2].ToString(), userInformationRow[3].ToString(), int.Parse(userInformationRow[4].ToString()), userInformationRow[5].ToString(), userInformationRow[6].ToString(), userInformationRow[7].ToString(), userInformationRow[8].ToString(), userInformationRow[9].ToString(), int.Parse(userInformationRow[10].ToString()));
        }
    }
}
