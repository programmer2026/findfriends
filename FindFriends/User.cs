﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindFriends
{
    class User
    {
        // Уникальный id пользователя
        public string TelegramId { get; set; }
        // Никнейм пользователя
        public string TelegramUsername { get; set; }
        // id чата, куда отправлять ответы
        public string TelegramChatId { get; set; }

        public string Name { get; set; }
        public int Age { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Gender { get; set; }
        public string Photo { get; set; }
        public string Description { get; set; }
        public int RegistrationStage { get; set; }

        public User(string telegramId, string telegramUsername, string telegramChatId, string name, int age, string county, string city, string gender, string photo, string description, int registrationStage)
        {
            TelegramId = telegramId;
            TelegramUsername = telegramUsername;
            TelegramChatId = telegramChatId;
            Name = name;
            Age = age;
            Country = county;
            City = city;
            Gender = gender;
            Photo = photo;
            Description = description;
            RegistrationStage = registrationStage;
        }

        public User(string telegramId, string telegramUsername, string telegramChatId)
        {
            TelegramId = telegramId;
            TelegramUsername = telegramUsername;
            TelegramChatId = telegramChatId;
        }
    }
}
