﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace FindFriends
{
    class Program
    {
        private static TelegramBotClient bot;

        static void Main(string[] args)
        {
            bot = new TelegramBotClient("");

            bot.OnMessage += Bot_OnMessageMain;
            bot.StartReceiving();

            Console.ReadLine();
        }

        static async void Bot_OnMessageMain(object sender, MessageEventArgs e)
        {
            if (e.Message.Text != null || e.Message.Photo.Length != 0)
            {
                // Текущее сообщение от пользователя
                Message message = e.Message;

                Console.WriteLine($"Received a text message in chat: {message.Chat.Id}.");
                Console.WriteLine($"Message: {message.Text}.");

                if (MyDataBase.IsUserRegistered(e.Message.From.Id.ToString()))
                {
                    await ShowRandomProfile(message);
                }
                else
                {
                    UserRegistration user = new UserRegistration(message.From.Id.ToString(), message.From.Username, message.Chat.Id.ToString());

                    string registrationStage = MyDataBase.ExecuteQueryWithAnswer($"SELECT registration_stage FROM User WHERE telegram_id = '{message.From.Id.ToString()}';");

                    if (registrationStage == null)
                    {
                        try
                        {
                            await bot.SendTextMessageAsync(
                                chatId: message.Chat.Id,
                                text: "Привет, как я вижу, у тебя ещё нет своей анкеты, давай я помогу тебе её сделать. Ответь на пару простых вопросов и сделай свою анкету."
                                );

                            Console.WriteLine("Creating a profile for the user {0}. ChatId: {1}", message.From.Username, message.Chat.Id);
                            await user.StartRegistration(bot);
                        }
                        catch { }
                    }
                    else
                    {
                        try
                        {
                            Console.WriteLine("Set parameters a profile for the user {0}. ChatId: {1}", message.From.Username, message.Chat.Id);
                            await user.SetParametersForUser(message.Text, message.Photo, int.Parse(registrationStage), bot);
                        }
                        catch { }
                    }
                }
                Console.WriteLine();
            }
        }

        public async static Task ShowRandomProfile(Message message)
        {
            User randomUser = MyDataBase.GetRandomUser();

            await bot.SendTextMessageAsync(
                chatId: message.Chat.Id,
                text: $"{randomUser.Name}, {randomUser.Gender}, {randomUser.Country}, {randomUser.City}, {randomUser.Age}\n\n{randomUser.Description}"
                );

            await bot.SendPhotoAsync(
                chatId: message.Chat.Id,
                photo: randomUser.Photo
                );
        }
    }
}
